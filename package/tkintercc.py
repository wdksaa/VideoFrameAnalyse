import os
import tkinter
from functools import partial

import chardet

from config import BASC
from package.Tkkinter_widget import CreatNormalWidget
from package.comic_test2 import comic_video_frame_analyse
from package.movie_test import movie_video_frame_analyse


class RecoverData():
    def write_file_from_get_frame(self,filename, str1):
        print(f'写入文件')


        fo = open(filename, "w+", encoding='utf-8')
        fo.write(str1)
        fo.close()
    #每个textframe的删除载入数据
    def input_temp_data(self,textframe,data):
        textframe.delete('1.0', "end")
        textframe.insert('insert', f'{data}')

    def get_code(self,filepath):
        with open(filepath,'rb') as f:
            return chardet.detect(f.read())['encoding']
    #顺序读取
    def read_content_order22a(self,filename):

        fk3 = open(filename, "r", encoding=self.get_code(filename))
        grafk = fk3.readlines()
        fk3.close()


        aaby=[]
        #去掉换行符
        # print(f'开头的去掉换行符')
        for ssij in grafk:
            ssij = str(ssij).replace("\n", "")
            # ssij = str(ssij).replace(" ", "")
            # ssij = str(ssij).replace("　", "")
            if len(ssij)>0:
                aaby.append(ssij)

        # aaby=no_repeat_list(aaby)
        return aaby

def out_picture_frame(tf,choic_num=0):
    redata=RecoverData()

    tframe1 = tf.get("1.0", "end").replace("\n","")
    video_dir=str(tframe1).replace("\\","\\\\")
    str_tol = f"{tframe1}"
    filename = f"{BASC}/icon/TempFrameData.txt"
    redata.write_file_from_get_frame(filename, str_tol)

    if len(video_dir) > 1:

        if choic_num==0:

            comic_video_frame_analyse(video_dir)
        else:
            if choic_num==1:

                movie_video_frame_analyse(video_dir)

            if choic_num == 2:
                picture_dir = f"{BASC}/output"
                os.startfile(picture_dir)
    else:
        print("没有输入视频路径，请输入视频路径再尝试")

# 创建一个按钮,这书蓝底的按钮
def create_button(main_win, h_numa, l_colu_numa, text1, command, width1=6, height1=1, bg="Wheat",columnspan=1,fg="MediumBlue"):
    if command is None:
        btn1 = tkinter.Button(main_win, text=f"{text1}", width=width1, height=height1,
                              # padx=10, pady=10,
                              bg=bg, font=("楷书", 13), fg=fg,
                              )

    else:
        btn1 = tkinter.Button(main_win, text=f"{text1}", width=width1, height=height1,
                              # padx=10, pady=10,
                              bg=bg, font=("楷书", 13), fg=fg,
                              # LightCyan，MediumBlue
                              command=command)
    btn1.grid(row=h_numa, column=l_colu_numa, padx=5, pady=10,columnspan=columnspan)
    return btn1, h_numa, l_colu_numa

class FramePictureGet():
    def __init__(self):
        self.classb = CreatNormalWidget()
        self.main_win = self.classb.creat_main_win("输出视频帧图", "videoframe.ico")
        self.textframe = self.classb.create_textframe_show2(self.main_win, 2, 1, 80, 4, columnspan=8)

        self.lable = self.classb.creat_lable(self.main_win, "视频所在文件夹", 1, 0, columnspan=2)

        self.__show_button()
        self.__get_tempframe_data()
        self.main_win.mainloop()

    def __show_button(self):
        self.search_button = create_button(self.main_win, 3, 1, "动漫视频截图帧\n2s",partial(out_picture_frame,self.textframe[0],choic_num=0), height1=2, width1=15,
                                           bg="Salmon",columnspan=2)
        self.search_button = create_button(self.main_win, 3, 3, "电影视频截图帧\n2s", partial(out_picture_frame,self.textframe[0],choic_num=1), height1=2, width1=15,
                                           bg="Wheat", columnspan=2)

        self.search_button = create_button(self.main_win, 3, 5, "打开\n截图输出文件夹",
                                           partial(out_picture_frame, self.textframe[0], choic_num=2), height1=2,
                                           width1=15,
                                           bg="DarkRed", columnspan=2, fg="Moccasin")

    def __get_tempframe_data(self):
        redata=RecoverData()
        path = f"{BASC}/icon/TempFrameData.txt"
        if os.path.exists(path):
            templist = redata.read_content_order22a(path)
            if len(templist) > 0:
                # input_temp_data(self.textframe3[0], templist[0])
                redata.input_temp_data(self.textframe[0], templist[0])

if __name__ == '__main__':
    FramePictureGet()
    # I:\VideoFrameAnalyse
    pass



