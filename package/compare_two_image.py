import os
import time

from natsort import ns, natsorted
import cv2
from skimage.metrics import structural_similarity as ssim
from package.hash_compare import *
from package.n_n_compare import have_file_compare_image_n_n
from package.sim_compare import have_file_compare_image_sim


def compare_image(xlcs,image1, image2,sa,sm):
    xlcsp=int(xlcs)+1
    img1 = cv2.imread(image1)
    img2 = cv2.imread(image2)

    hist1B = cv2.calcHist([img1], [0], None, [256], [0, 255])
    hist2B = cv2.calcHist([img2], [0], None, [256], [0, 255])
    retvalB = cv2.compareHist(hist1B, hist2B, cv2.HISTCMP_CORREL)

    hist1G = cv2.calcHist([img1], [1], None, [256], [0, 255])
    hist2G = cv2.calcHist([img2], [1], None, [256], [0, 255])
    retvalG = cv2.compareHist(hist1G, hist2G, cv2.HISTCMP_CORREL)

    hist1R = cv2.calcHist([img1], [2], None, [256], [0, 255])
    hist2R = cv2.calcHist([img2], [2], None, [256], [0, 255])
    retvalR = cv2.compareHist(hist1R, hist2R, cv2.HISTCMP_CORREL)

    image1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)  # 将图像转换为灰度图
    image2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)  # 将图像转换为灰度图
    sim = ssim(image1, image2)

    hash1 = aHash(img1, xlcs)
    hash2 = aHash(img2, xlcs)
    n1 = cmpHash(hash1, hash2)

    hash1 = dHash(img1, xlcs, xlcsp)
    hash2 = dHash(img2, xlcs, xlcsp)
    n2 = cmpHash(hash1, hash2)



def have_file_compare_image(image_file,compare_rate,which_num):


    if which_num==1:
        the_sim=have_file_compare_image_sim(image_file,compare_rate)
        print(f"建议的sim：{the_sim}")
    if which_num==2:
        the_n1,the_n2=have_file_compare_image_n_n(image_file, compare_rate)
        print(f"建议的n1,n2：{the_n1},{the_n2}")



if __name__ == '__main__':
    pass