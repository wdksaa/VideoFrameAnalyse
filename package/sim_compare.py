import os
import time

from natsort import ns, natsorted
import cv2
from skimage.metrics import structural_similarity as ssim
from package.hash_compare import *

"""
sim_compare,就是剔除掉结构相似的图片，取的是最小的sim值

"""


def compare_image_sim(xlcs,image1, image2,sa,sm):
    xlcsp=int(xlcs)+1
    img1 = cv2.imread(image1)
    img2 = cv2.imread(image2)

    image1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)  # 将图像转换为灰度图
    image2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)  # 将图像转换为灰度图
    sim = ssim(image1, image2)

    return sim
    # print("推荐参数：")

def take_min(list):
    the_min=None
    for aa in range(len(list)):
        num=list[aa]
        if the_min is None:
            the_min=float(num)
        else:
            if float(num)<float(the_min):
                the_min = float(num)
    return the_min


def have_file_compare_image_sim(image_file,compare_rate):

    #储存参数的数组
    resul_list=[]
    # 文件夹
    pathy = image_file

    # 给文件夹的文件进行排序
    files = os.listdir(pathy)
    files = natsorted(files, alg=ns.PATH)

    # flen = len(files)
    for ilf in range(len(files)):
        for ilf2 in range(len(files)):
            if ilf2 > ilf:
                frame = pathy + '/' + files[ilf]
                j = pathy + '/' + files[ilf2]

                sim_result=compare_image_sim(compare_rate,frame, j, files[ilf], files[ilf2])
                resul_list.append(sim_result)
    the_min=take_min(resul_list)
    return the_min


if __name__ == '__main__':
    pass
