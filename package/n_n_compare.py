import os
import time

from natsort import ns, natsorted
import cv2
from skimage.metrics import structural_similarity as ssim
from package.hash_compare import *

"""
n_n_compare,筛选不同的图片，让它们不能去掉，取的是最小的n1，n2值

"""


def compare_image_n_n(xlcs, image1, image2, sa, sm):
    xlcsp = int(xlcs) + 1
    img1 = cv2.imread(image1)
    img2 = cv2.imread(image2)

    hash1 = aHash(img1, xlcs)
    hash2 = aHash(img2, xlcs)
    n1 = cmpHash(hash1, hash2)

    hash1 = dHash(img1, xlcs, xlcsp)
    hash2 = dHash(img2, xlcs, xlcsp)
    n2 = cmpHash(hash1, hash2)

    return n1,n2
    # print("推荐参数：")


def take_min_n_n(list):
    the_min = None
    for aa in range(len(list)):
        num = list[aa]
        if the_min is None:
            the_min = int(num)
        else:
            if int(num) < int(the_min):
                the_min = int(num)
    return the_min


def have_file_compare_image_n_n(image_file, compare_rate):

    # 储存参数的数组
    resul_n1_list = []
    resul_n2_list = []
    # 文件夹
    pathy = image_file

    # 给文件夹的文件进行排序
    files = os.listdir(pathy)
    files = natsorted(files, alg=ns.PATH)

    # flen = len(files)
    for ilf in range(len(files)):
        for ilf2 in range(len(files)):
            if ilf2 > ilf:
                frame = pathy + '/' + files[ilf]
                j = pathy + '/' + files[ilf2]

                n1_result,n2_result = compare_image_n_n(compare_rate, frame, j, files[ilf], files[ilf2])
                resul_n1_list.append(n1_result)
                resul_n2_list.append(n2_result)
    the_min_n1 = take_min_n_n(resul_n1_list)
    the_min_n2 = take_min_n_n(resul_n2_list)
    return the_min_n1,the_min_n2


if __name__ == '__main__':
    pass
