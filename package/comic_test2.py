from package.detect_code import GetPicture

def comic_video_frame_analyse(vdop):
    pathy =vdop

    #选定为2，因为你已经调试好了，可以直接按这个输出
    test_num=2

    #然后不需要compare，及其参数了。

    #选定为2,2秒钟匹配一张。
    timefsec = 2

    dict = {"n1": 18, "n2": 27,
            "retvalB": 0.9, "retvalR": 0.9, "retvalG": 0.9,
            "sim": 0.6 ,
            "xsdnmaa": 8,
            "test_num": test_num
            }
    getp=GetPicture(n1=dict.get("n1"),n2=dict.get("n2"),
                    retvalB=dict.get("retvalB"),retvalR=dict.get("retvalR"),retvalG=dict.get("retvalG"),
                    sim=dict.get("sim"),xsdnmaa=dict.get("xsdnmaa"),timefsec=timefsec,videopath=pathy)

    getp.get_picture(dict.get("test_num"))

if __name__ == '__main__':
    pass