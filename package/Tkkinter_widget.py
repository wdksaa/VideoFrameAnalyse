import tkinter

from config import BASC


class CreatNormalWidget():
    #创建一个main_win
    def creat_main_win(self,title_pl,icon_name,frame_width=605,frame_height=770):
        title_pl = title_pl

        main_win = tkinter.Tk()
        main_win.iconbitmap(f'{BASC}/icon/{icon_name}')

        # 窗口的大小
        frame_width = frame_width
        frame_height = frame_height
        # 调整窗口位置
        main_win.geometry(f"{frame_width}x{frame_height}")
        main_win.geometry("+500+5")
        # 设置窗口标题
        main_win.title(f"{title_pl}")
        return main_win


    #这是文本框textframe的,这是有滚动条的
    def create_textframe_show(self,main_win,h_numa,l_colu_numa,width1,height1,columnspan=8):
        textframe = tkinter.Text(main_win, exportselection=False, fg='#2a2a2a', bg='white',height=height1,width=width1,
                                 insertontime=0)

        ybar = tkinter.Scrollbar(main_win)
        # event_text = tk.Text(root, height=10, width=10)
        ybar.config(command=textframe.yview)
        textframe.config(yscrollcommand=ybar.set)

        textframe.grid(row=h_numa, column=l_colu_numa, columnspan=columnspan, padx=5, pady=5)
        l_colu_numa = l_colu_numa + 1
        ybar.grid(row=h_numa, column=columnspan+1, sticky="ns")
        textframe.config(selectbackground="white", foreground="Black")

        return textframe,h_numa,l_colu_numa

    # 这是文本框textframe的,这是有滚动条的
    def create_textframe_show2(self, main_win, h_numa, l_colu_numa, width1, height1, columnspan=8):
        textframe = tkinter.Text(main_win, exportselection=False, fg='#2a2a2a', bg='white', height=height1,
                                 width=width1)

        ybar = tkinter.Scrollbar(main_win)
        # event_text = tk.Text(root, height=10, width=10)
        ybar.config(command=textframe.yview)
        textframe.config(yscrollcommand=ybar.set)

        textframe.grid(row=h_numa, column=l_colu_numa, columnspan=columnspan, padx=5, pady=5)
        l_colu_numa = l_colu_numa + 1
        ybar.grid(row=h_numa, column=columnspan + 1, sticky="ns")
        # textframe.config(selectbackground="white", foreground="Black")

        return textframe, h_numa, l_colu_numa

    def create_textframe_show3(self, main_win, h_numa, l_colu_numa, width1, height1,columnspan=8):
        textframe = tkinter.Text(main_win, exportselection=False, fg='#2a2a2a', bg='white', height=height1,
                                 width=width1)

        # ybar = tkinter.Scrollbar(main_win)
        # # event_text = tk.Text(root, height=10, width=10)
        # ybar.config(command=textframe.yview)
        # textframe.config(yscrollcommand=ybar.set)

        textframe.grid(row=h_numa, column=l_colu_numa, columnspan=columnspan, padx=5, pady=5)
        l_colu_numa = l_colu_numa + 1
        # ybar.grid(row=h_numa, column=column2, sticky="ns")
        # textframe.config(selectbackground="white", foreground="Black")

        return textframe, h_numa, l_colu_numa

    #这是创建一个lable的
    def creat_lable(self,main_win,text1,h_numa,l_colu_numa,columnspan=0):
        lab1=tkinter.Label(main_win, text=f"{text1}：").grid(row=h_numa, column=l_colu_numa,columnspan=columnspan)

        return lab1,h_numa,l_colu_numa

    #创建一个按钮,这书蓝底的按钮
    def __create_button(self,main_win,h_numa,l_colu_numa,text1,command,width1=6,height1=1,bg="Wheat"):
        if command is None:
            btn1 = tkinter.Button(main_win, text=f"{text1}", width=width1, height=height1,
                                  # padx=10, pady=10,
                                  bg=bg, font=("楷书", 11), fg="MediumBlue",
                                  )

        else:
            btn1 = tkinter.Button(main_win, text=f"{text1}", width=width1, height=height1,
                                  # padx=10, pady=10,
                                  bg=bg, font=("楷书", 11), fg="MediumBlue",
                                  # LightCyan，MediumBlue
                                  command=command)
        btn1.grid(row=h_numa, column=l_colu_numa, padx=5, pady=10)
        return btn1,h_numa,l_colu_numa

    #创建一个蓝底的按钮
    def creat_l_bg_button(self,main_win, h_numa, l_colu_numa, text1,width=6, height=1, command=None, bg="LightCyan"):
        btn1,h_numa,l_colu_numa=self.__create_button(main_win, h_numa, l_colu_numa, text1,command,width1=width, height1=height,  bg=bg)
        return btn1, h_numa, l_colu_numa

    #创建一个褐色底的按钮
    def creat_brown_bg_button(self,main_win, h_numa, l_colu_numa, text1,width=6, height=1, command=None, bg="Wheat"):
        btn1,h_numa,l_colu_numa=self.__create_button(main_win, h_numa, l_colu_numa, text1,command,width1=width, height1=height,  bg=bg)
        return btn1, h_numa, l_colu_numa

    #创建一个输入框
    def creat_entry(self,main_win,default_num2,h_numa, l_colu_numa,width=10):
        default_num = tkinter.StringVar(value=default_num2)
        entry1= tkinter.Entry(main_win, textvariable=default_num,width=width)
        # self.entry1= tkinter.Entry(self.main_win,width=10)
        entry1.grid(row=h_numa, column=l_colu_numa, padx=2, pady=2,)
        return entry1, h_numa, l_colu_numa

    def add_close_button(self,main_win, h_numa, l_colu_numa,columnspan):
        btn1 = tkinter.Button(main_win, text=f"关闭窗口", width=15, height=3,
                                               # padx=10, pady=10,
                                               bg="Lavender", font=("楷书", 11), fg="DeepSkyBlue",
                                               command=main_win.destroy())
        btn1.grid(row=h_numa, column=1, padx=10, pady=20, columnspan=columnspan)
        # l_colu_numa = l_colu_numa + 1
        # if l_colu_numa == 4:
        #     l_colu_numa = 0
        #     h_numa = h_numa + 1
        return btn1,h_numa, l_colu_numa



#这是创建一个用来显示的文本框类型的显示框，功能类似于labke之类的
class CreatShowEntry():

    def __init__(self,main_win,h_numa, l_colu_numa,width=10,height=1):
        self.textentry,self.h_numa, self.l_colu_numa=self.__creat_show_entry(main_win,h_numa, l_colu_numa,width=width,height=height)


    #可以删除的显示框
    def __creat_show_entry(self,main_win,h_numa, l_colu_numa,width=10,height=1):
        entry12 = tkinter.Text(main_win, width=width, height=height, insertontime=0)
        entry12.config(selectbackground="white", foreground="Black")
        entry12.grid(row=h_numa, column=l_colu_numa, padx=2, pady=2)
        return entry12, h_numa, l_colu_numa



    #可以删除的文本框的设置值
    def __set_value(self,value1):
        self.textentry.delete('1.0', "end")
        # labss.delete(0, "end")
        self.textentry.insert('insert', value1)
        self.textentry.tag_add("txtformxxd", '1.0', "end")
        # cc = cc + len(nn) + 2
        self.textentry.tag_config(f"txtformxxd", font=("等线", 10), selectbackground="white", foreground="Black")

    def get_text_normal_data(self):
        return self.textentry,self.h_numa, self.l_colu_numa


    def set_value_data(self,value1):
        self.__set_value(value1)

if __name__ == '__main__':
    pass